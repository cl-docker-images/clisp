#!/bin/sh

# If the first arg starts with a hyphen, prepend clisp to arguments.
if [ "${1#-}" != "$1" ]; then
	set -- clisp "$@"
fi

exec "$@"
