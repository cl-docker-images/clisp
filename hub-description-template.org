#+TITLE: CLISP Docker Images
#+AUTHOR: Eric Timmons

* Supported Tags

** Simple Tags

   INSERT-SIMPLE-TAGS

** Shared Tags

   INSERT-SHARED-TAGS

* Quick Reference

  + CLISP Home Page :: [[https://clisp.sourceforge.io/]]
  + Where to file Docker image related issues :: [[https://gitlab.common-lisp.net/cl-docker-images/clisp]]
  + Where to file issues for CLISP itself :: [[https://gitlab.com/gnu-clisp/clisp/]]
  + Maintained by :: [[https://common-lisp.net/project/cl-docker-images][CL Docker Images Project]]
  + Supported platforms :: =linux/amd64=, =linux/arm64/v8=, =linux/arm/v7=

* What is CLISP?

  From [[https://clisp.sourceforge.io/summary.html][CLISP's Home Page]]:

  #+begin_quote
  CLISP is a Common Lisp implementation by Bruno Haible, then of Karlsruhe
  University, and Michael Stoll, then of Munich University, both in Germany. It
  implements the language described in the ANSI Common Lisp standard with many
  extensions.

  CLISP includes an interpreter, a compiler, a debugger, CLOS, MOP, a foreign
  language interface, i18n, POSIX and Perl regular expressions, a socket
  interface, fast bignums, arbitrary precision floats, and more. An X11
  interface is available through CLX, Garnet and CLUE/CLIO. Command line
  editing is provided by readline. CLISP runs Maxima, ACL2 and many other
  Common Lisp packages.
  #+end_quote

* How to use this image

** Create a =Dockerfile= in your CLISP project

   #+begin_src dockerfile
     FROM %%IMAGE%%:latest
     COPY . /usr/src/app
     WORKDIR /usr/src/app
     CMD [ "clisp", "-c", "-l", "./your-daemon-or-script.lisp" ]
   #+end_src

   You can then build and run the Docker image:

   #+begin_src console
     $ docker build -t my-clisp-app
     $ docker run -it --rm --name my-running-app my-clisp-app
   #+end_src

** Run a single Common Lisp script

   For many simple, single file projects, you may find it inconvenient to write
   a complete =Dockerfile=. In such cases, you can run a Lisp script by using
   the CLISP Docker image directly:

   #+begin_src console
     $ docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest clisp -c -l ./your-daemon-or-script.lisp
   #+end_src

** Developing using SLIME

   [[https://common-lisp.net/project/slime/][SLIME]] provides a convenient and fun environment for hacking on Common
   Lisp. To develop using SLIME, first start the Swank server in a container:

   #+begin_src console
     $ docker run -it --rm --name clisp-slime -p 127.0.0.1:4005:4005 -v /path/to/slime:/usr/src/slime -v "$PWD":/usr/src/app -w /usr/src/app %%IMAGE%%:latest clisp -x "(load /usr/src/slime/swank-loader.lisp)" -x '(swank-loader:init)' -x '(swank:create-server :dont-close t :interface "0.0.0.0")'
   #+end_src

   Then, in an Emacs instance with slime loaded, type:

   #+begin_src emacs
     M-x slime-connect RET RET RET
   #+end_src


* Image variants

  This image comes in several variants, each designed for a specific use case.

** =%%IMAGE%%:<version>=

   This is the defacto image. If you are unsure about what your needs are, you
   probably want to use this one. It is designed to be used both as a throw
   away container (mount your source code and start the container to start your
   app), as well as the base to build other images off of. Additionally, these
   images contain the CLISP source code (at =/usr/local/src/clisp=) to help
   facilitate interactive development and exploration (a hallmark of Common
   Lisp!).

   Some of these tags may have names like buster or stretch in them. These are
   the suite code names for releases of Debian and indicate which release the
   image is based on. If your image needs to install any additional packages
   beyond what comes with the image, you'll likely want to specify one of these
   explicitly to minimize breakage when there are new releases of Debian.

   These images are built off the buildpack-deps image. It, by design, has a
   large number of extremely common Debian packages.

   These images contain the Quicklisp installer, located at
   =/usr/local/share/common-lisp/source/quicklisp/quicklisp.lisp=. Additionally,
   there is a script at =/usr/local/bin/install-quicklisp= that will use the
   bundled installer to install Quicklisp. You can configure the Quicklisp
   install with the following environment variables:

   + =QUICKLISP_DIST_VERSION= :: The dist version to use. Of the form
     yyyy-mm-dd. =latest= means to install the latest version (the default).
   + =QUICKLISP_CLIENT_VERSION= :: The client version to use. Of the form
     yyyy-mm-dd. =latest= means to install the latest version (the default).
   + =QUICKLISP_ADD_TO_INIT_FILE= :: If set to =true=, =(ql:add-to-init-file)=
     is used to add code to the implementation's user init file to load
     Quicklisp on startup. Not set by default.

   Additionally, these images contain cl-launch to provide a uniform interface
   to running a Lisp implementation without caring exactly which implementation
   is being used (for instance to have uniform CI scripts).


* License

  CLISP is licensed under the GPLv2. See CLISP's [[https://gitlab.com/gnu-clisp/clisp/-/blob/master/COPYRIGHT][COPYRIGHT]] file for more info.

  The Dockerfiles used to build the images are licensed under BSD-2-Clause.

  As with all Docker images, these likely also contain other software which may
  be under other licenses (such as Bash, etc from the base distribution, along
  with any direct or indirect dependencies of the primary software being
  contained).

  As for any pre-built image usage, it is the image user's responsibility to
  ensure that any use of this image complies with any relevant licenses for all
  software contained within.
