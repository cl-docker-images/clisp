# Unfortunately clisp has not made a release tarball in over 10 years (!?), so
# we have to check out the source code from git.

FROM PLACEHOLDER

# hadolint ignore=DL3008
RUN set -x \
    && apt-get update \
    && apt-get install --no-install-recommends -y cl-launch \
    && rm -rf /var/lib/apt/lists/*

# CLISP determines the default encoding from LANG. Set to something likely to
# be sane.
ENV LANG C.UTF-8

ENV CLISP_VERSION PLACEHOLDER

WORKDIR /usr/local/src/

# hadolint ignore=DL3003,DL3008
RUN set -x \
    && case "$(dpkg --print-architecture)" in \
         armhf|arm64) CONFIGURE_ARGS="--enable-portability --with-unicode --with-module=rawsock --with-module=bindings/glibc --with-module=pcre --with-module=zlib --with-module=asdf";; \
         *) CONFIGURE_ARGS="--enable-portability --with-unicode --with-threads=POSIX_THREADS --with-module=rawsock --with-module=bindings/glibc --with-module=pcre --with-module=zlib --with-module=asdf";; \
      esac \
    && case "$CLISP_VERSION" in \
       2.49.92) CLISP_TAG=clisp-2.49.92-2018-02-18;; \
       *) echo "Unknown version" >&2; exit 1;; \
    esac \
    && export CLISP_TAG \
    && case "$(grep "^VERSION_CODENAME=" /etc/os-release | tail -c +18)" in \
         bullseye|buster) PACKAGES="libffcall-dev libsigsegv-dev";; \
         stretch) PACKAGES="libffcall1-dev libsigsegv-dev";; \
         *) echo "Unknown OS" >&2; exit 1;; \
       esac \
    && apt-get update \
    && apt-get install --no-install-recommends -y $PACKAGES \
    && curl -fsSL https://gitlab.com/gnu-clisp/clisp/-/archive/$CLISP_TAG/clisp-$CLISP_TAG.tar.gz > clisp-${CLISP_TAG}.tgz \
    && mkdir clisp \
    && tar -C clisp -xf clisp-${CLISP_TAG}.tgz --strip-components=1 \
    && cd clisp \
    && FORCE_UNSAFE_CONFIGURE=1 ./configure $CONFIGURE_ARGS \
    && make \
    && make install \
    && make distclean \
    && cd .. \
    && rm -r clisp-${CLISP_TAG}.tgz \
    && rm -rf /var/lib/apt/lists/* \
    && clisp --version

# Add the Quicklisp installer.
WORKDIR /usr/local/share/common-lisp/source/quicklisp/

ENV QUICKLISP_SIGNING_KEY D7A3489DDEFE32B7D0E7CC61307965AB028B5FF7

RUN set -x \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp" > quicklisp.lisp \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp.asc" > quicklisp.lisp.asc \
    && GNUPGHOME="$(mktemp -d)" \
    && export GNUPGHOME \
    && gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "${QUICKLISP_SIGNING_KEY}" \
    && gpg --batch --verify "quicklisp.lisp.asc" "quicklisp.lisp" \
    && rm quicklisp.lisp.asc \
    && rm -rf "$GNUPGHOME"

# Add the script to trivially install Quicklisp
COPY install-quicklisp /usr/local/bin/install-quicklisp

# Add the entrypoint
WORKDIR /

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["clisp"]
